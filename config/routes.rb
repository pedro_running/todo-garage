Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'app#index'

  #post '/projects', to: 'projects#create'
  resources :projects do
  	resources :tasks 
  end

  get '/projects/new', to: 'projects#new'

  get '/login', to: 'sessions#new'
  post '/logout', to: 'sessions#destroy' #get?
  get '/register', to: 'users#new'

  post '/users', to: 'users#create'
  post '/sessions', to: 'sessions#create'

end
