class CreateTasks < ActiveRecord::Migration[5.1]
  def change
    create_table :tasks do |t|
      t.text :text
      t.boolean :status
      t.date :deadline
      t.references :project, foreign_key: true

      t.timestamps
    end
  end
end
