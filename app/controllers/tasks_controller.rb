class TasksController < ApplicationController
  before_action :authorize, only: [:destroy, :update]

	def create
		@project = Project.find(params[:project_id])
		@task = @project.tasks.create(text: params[:text], status: false)

    respond_to do |format|
      format.js
    end
	end

  def destroy
    @task = Task.find(params[:id])
    @task.destroy

    respond_to do |format|
      format.js
    end
  end

  def update
    @task = Task.find(params[:id])
    if params[:status]
      @task.update(status: params[:status])
      render 'status.js.erb'
    elsif params[:edit]
      render 'edit.js.erb'
    elsif params[:text]
      @task.update(text: params[:text])
      render 'show.js.erb'
    end
    respond_to do |format|
      format.js
    end
  end

  private
    def authorize
      if Task.find(params[:id]).project.user.id != @current_user.id
        redirect_to '/login'
      end
    end
end
