class AppController < ApplicationController

	def index
		if @current_user
		  @projects = Project.where(user_id: @current_user.id)
		end
  end
end
