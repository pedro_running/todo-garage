class UsersController < ApplicationController
	def new
	end

	def create
		if @user = User.create(login: params[:login], 
			                  password: params[:password])
                    .valid?
      flash[:success] = 'User created'
    else
      flash[:error] = 'Login already exists'
    end
    redirect_to '/'
	end
end
