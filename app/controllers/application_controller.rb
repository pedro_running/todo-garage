class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :get_current_user

  private
    def get_current_user
    	if session[:user_id]
    		@current_user = User.find_by_id(session[:user_id])
        if @current_user.nil? #fallback for when cookie is present but user is deleted
          session[:user_id] = nil 
        end
    	end
    end
end
