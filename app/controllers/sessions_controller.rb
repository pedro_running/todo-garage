class SessionsController < ApplicationController
	def new	
	end

	def create
		user = User.find_by(login: params[:login])
		if user 
			@user = user.authenticate(params[:password])
			if @user
				session[:user_id] = @user.id
				redirect_to '/'
			else #TODO: DRY
				flash[:alert] = "Wrong user or password"
				redirect_to '/login'
			end
		else
			flash[:alert] = "Wrong user or password"
			redirect_to '/login'
		end
	end

	def destroy
		session[:user_id] = nil
		redirect_to '/'
	end
end
