class ProjectsController < ApplicationController
  before_action :authorize, only: [:destroy, :update] #create?

  def new
    respond_to do |f|
      f.js 
    end
  end

	def create
		@project = Project.create(name: params[:name], user_id: @current_user.id)
		
    respond_to do |f|
      f.js 
    end
	end

  def destroy
    @project = Project.find(params[:id])
    @project.destroy
    respond_to do |f|
      f.js 
    end 
  end 

  def update
    @project = Project.find(params[:id])
    if params[:edit]
      render 'edit.js.erb'
    end
    if params[:name]
      @project.update(name: params[:name])
      render 'show.js.erb'
    end
  end

  private
    def authorize
      if @current_user.id != Project.find(params[:id]).user_id
        redirecto_to '/login'
      end
    end
end
