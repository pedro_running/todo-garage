class User < ApplicationRecord
  validates :login, uniqueness: { case_sensitive: false }
	has_many :projects, dependent: :destroy
	has_secure_password
end
